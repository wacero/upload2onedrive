import os
import upload2onedrive_utils as utils
import logging
from O365 import Account
from O365 import connection 
from O365.utils import FileSystemTokenBackend as FSToken 

def get_account(account_name,credential_file):
    """
    Reads the credentials in a json file and return an account object

    :param string account_name: account name as in the credential.json fle
    :param string credential_file: path to the credential.json file
    :returns: 0365 account object
    """
    logging.info("starting get_account")
    credentials=utils.read_json_file(credential_file)

    try:
        user=credentials[account_name]
        user_credentials=('%s' %user['client_id'],'%s'%user['client_secret'])
        token_path="%s" %user['token_file']
        
    except Exception as e:
        logging.error("Failed to get credential and token file: %s" %str(e))
    
    try:
        token_object=FSToken(token_path)
    except Exception as e:
        logging.error("Failed to get token file:%s" %str(e))

    try:       
        return Account(user_credentials,token_backend=token_object)
    except Exception as e:
        logging.error("Error in get_account, error was:%s" %str(e))


def get_drive(account):
    """
    Returns a drive object
    
    :param 0365.account object account
    :returns O365.drive object 
    """
    logging.info("starting get_drive")
    try:
        return account.storage().get_default_drive()
    except Exception as e:
        logging.error("Error in get_drive, error was:%s" %str(e))


def get_document_folder(drive):
    """
    Return the well known onedrive folder DOCUMENTS

    :param 0365.drive object 
    :returns 0365.folder documents
    """

    try:
        return drive.get_special_folder("documents")
    except Exception as e:
        logging.error("Error in get_document_folder, error was:%s" %str(e))

def get_root_folder(drive):
    """
    Return the onedrive folder ROOT

    :param 0365.drive object 
    :returns 0365.folder ROOT
    """

    try:
        return drive.get_root_folder()
    except Exception as e:
        logging.error("Error in get_root_folder,error was: %s" %str(e))

def get_list_files(data_path):
    """
    Return a list of file paths

    :param string data_path:  path of a local folder with files to upload
    :returns list of absolute filepath
    """

    files_list=[]
    try:
        for r,d,f in os.walk(data_path):
            for item in f:
                files_list.append(os.path.join(r,item))    
        return files_list
    except Exception as e:
        logging.error("Error in get_list_files, error was: %s" %str(e))


def _get_child_folder( folder, child_folder_name):
    """
    Function that finds a child folder inside a folder or creates it
    
    :param O365.folder object 
    :param string child_folder_name: path of folder separated by /
    :returns O365.folder as indicated by child_folder_name

    """
    child_folder_names = [(item.name) for item in folder.get_items() if item.is_folder]
    if child_folder_name in child_folder_names:
        
        return list(filter(lambda x: x.name == child_folder_name, folder.get_items()))[0]
        
    else:
        return folder.create_child_folder(child_folder_name)

def get_child_folder( folder, child_folder_name):
    """
    Function that calls _get_child_folder 

    :param O365.folder object 
    :param string child_folder_name: path of folder separated by /
    :returns O365.folder as indicated by child_folder_name
    """

    child_folder_names = child_folder_name.split('/')
    for _child_folder_name in child_folder_names:
        folder = _get_child_folder(folder, _child_folder_name)
    return folder

def upload_files(cloud_folder,file_list):
    """
    Function that uploads a file_list to a onedrive cloud folder

    :param 0365.folder cloud_folder: O365 folder to upload data
    :param list file_list: absolute path of files to upload
    """
    logging.info("Starting upload_files")
    for temp_file in file_list:
        try:
            cloud_folder.upload_file(item='%s' %temp_file)
        except Exception as e:
            logging.error("Error in upload files: %s" %str(e))

"""
credential_file="./credentials.json"
account_name="SHAKEMAP"
cloud_folder_name="SHAKEMAP/igepn2020abcd"
local_folder_path="/home/wacero/igepn2020abcd"

cuenta=get_account(credential_file)
drive=get_drive(cuenta)
root_folder=get_root_folder(drive)
cloud_folder=get_child_folder(root_folder,cloud_folder_name)
files2upload=get_list_files(local_folder_path)
upload_files(cloud_folder,files2upload)
print("Fin del codigo")
"""


