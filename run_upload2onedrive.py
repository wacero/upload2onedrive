

import os, sys
import logging
import upload2onedrive

def main():
    """
    This script runs the upload2onedrive module
    """
    exec_dir=os.path.realpath(os.path.dirname(__file__))
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',filename=os.path.join(exec_dir,"nube_igepn.log"),level=logging.INFO)

    is_error=False

    if len(sys.argv)==1:
        is_error=True

    else:
        try:
            logging.info("Start of upload2onedrive")
            credential_file="%s" %sys.argv[1]
            account_name="%s" %sys.argv[2]
            cloud_folder_name= "%s" %sys.argv[3]
            local_folder_path= "%s" %sys.argv[4]
        except Exception as e:
            logging.error("Error reading input arguments: %s" %str(e))
        
        try:
            cuenta = upload2onedrive.get_account(account_name,credential_file)
            drive = upload2onedrive.get_drive(cuenta)
            root_folder = upload2onedrive.get_root_folder(drive)
            cloud_folder=upload2onedrive.get_child_folder(root_folder,cloud_folder_name)
            files2upload=upload2onedrive.get_list_files(local_folder_path)
            upload2onedrive.upload_files(cloud_folder,files2upload)

        except Exception as e:
            logging.error("Error uploading to cloud: %s" %str(e))
            
    if is_error:
        
        logging.error(f'Usage: python {sys.argv[0]} credential_file account_name cloud_folder_name local_folder_path ')
        print(f'Usage: python {sys.argv[0]} credential_file account_name cloud_folder_name local_folder_path ')


main()

